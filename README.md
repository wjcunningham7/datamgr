Data Manager
============

## Summary
This repository contains a set of bash tools used to create and manage statistical datasets. Users may find this useful in the following scenarios:

- You want to run your code using a series of different parameters and easily manage output data
- You want to run your code on more than one cluster without manually assigning tasks (meta-scheduling)
- You want to run more than one program on a cluster while using a consistent naming scheme
- You want to easily re-run old experiments which may have generated corrupted data (data integrity verification)
- You want to generate and analyze multiple datasets while staying organized

The tools enclosed primarily use bash. Users are recommended to store their datasets in the HDF5 file format so that metadata is easily accessed.

## Installation
Run the script 'install' and follow the prompts. The script will add some environment variable definitions to your personal .bashrc file, which you must reload after installation. If you want to install system-wide, you can manually add these to the file /etc/environment.d/90-dmgr-env.conf.

Installation will create a sample project called 'ABC' which you can edit according to your needs. See below for details on individual files.

## Usage
Run 'dmgr' without any arguments to see the help menu.

Projects are assigned three- or four-letter codes used in file naming schemes, e.g. ABC. Datasets (experiments) within a project are numbered sequentially, e.g. ABC-0. Individual samples are assigned a number as well, e.g. ABC-0-0. Different samples may refer to a simulation run at different sizes, or with different initial conditions, while different datasets address different research questions.

### Directories
There are several important directories referenced below.

- *DATAMGR\_ROOT* is the installation directory of this toolkit. It contains the main bash scripts and also a subdirectory 'prj' which contains project configuration files. This environment variable is set when the installation script is run.

- *DATAMGR\_PROJ* is the directory containing your projects' source code. It should only contain directories, each one corresponding to a single project. This environment variable is set when the installation script is run. However, users also must specify this on all remote machines specified in the file '$DATAMGR\_ROOT/clusters.par'

- *DATAMGR\_DATA* refers to the location of your data vault. Each subdirectory, specified by a project code, will contain data for one project.

- *SCRATCH* is the directory on the remote machine where data is generated. This environment variable must be manually set by users on each remote machine specified in '$DATAMGR\_ROOT/clusters.par'

### Project Creation
A project refers to the data associated with a codebase, and it is assigned (by the user) a three- or four-letter project code used in the naming of files and directories. The files which contains project parameters are found in $DATAMGR\_ROOT/prj, and they each contain the following parameters:

- *proj* is the three- or four-letter code assigned to the project.
- *projname* is the name of the directory within $DATAMGR\_PROJ that contains the source code and project binaries.
- *dataname* is the directory pattern of the generated data. It may be the same as 'proj' or 'projname' or something else.
- *remote* is the hostname of the remote server.
- *server* is the hostname of the file transfer server used by 'remote'. If there is not a file transfer server, it is the same as 'remote'.
- *remoteproj* is the directory on the remote machine analogous to $DATAMGR\_PROJ.
- *localproj* is the local project directory set by $DATAMGR\_PROJ.
- *datadir* is the directory where data for the project is stored.
- *gpu* is set to true/false to indicate whether GPUs will be used.
- *gputype* specifies the type of GPUs requested.
- *gpunum* specifies the number of GPUs requested.
- *cores* specifies the number of cores per job requested.
- *partition* is the name of one or more SLURM partitions requested.
- *currentmachine* is the location of the most up-to-date code, either localhost or one of the remotes.
- *currentpartition* is the name of the SLURM partition on 'remote' being used.
- *currentexperiment* is the numerical identifier of the dataset being generated or studied.

Run the command 'dmgr [CODE] showconfig' to see the current values of these parameters for a particular project.

### Data Generation
The general pipeline for data generation is the following:

1. Create parameter files for a new project and/or dataset
2. Move the code in the local projects directory to the remote projects directory
3. Generate the data on the remote cluster
4. Compress and transfer the generated data back to the local machine
5. Analyze the data on the local machine
6. Move the code from the remote machine back to the local machine

At any point, the user may run 'dmgr [CODE] status' to see where in the pipeline the current status of the project.

#### Data Pipeline
Run the following commands in order to push data through the pipeline: *dmgr [CODE] [COMMAND]*

1. *new* - Create a new experiment for a project. If this is the first experiment, e.g., for a new project, use the command *newproject* instead.
2. *select* - Check available resources on the clusters specified in the file clusters.par.
3. *setremote [remote] [xferserver]* - Set the remote server you will use. The last argument, which specifies a file transfer server, is optional.
4. *setpartition [partition]* - Set the SLURM partition that will be used on the remote server. This is used for accounting purposes only, and it does not specify the partition used in SLURM scripts on the remote server.
5. *mkpar* - Create parameter files by calling *genpar_hook* and the experiment's *genpar* script.
6. *push* - Push source code files to the remote server.
7. *syncpars* - Synchronize the parameter files. After this stage, users should ssh to the remote machine and submit jobs to SLURM. Make sure output data is stored in HDF5 format with file extension "h5". See files *sbatch_input*, *sbatch_wrapper*, and *slurm_test* for examples for interfacing with SLURM. Wait until all jobs are completed before proceeding.
8. *compress* - Compress the resulting data on the remote machine. See the file *compress* for an example. This should check error logs and create a compressed file in the tar.xz format for jobs without errors.
9. *pulldata* - Pull the compressed data files from the remote machine to your local machine. If you are merging the data with previously created data, use the command *pulldata merge*.
10. *merge* - Merge new and old HDF5 files using h5merge. Double check that this works for your specific use case!
11. *missing* - Check for missing files. If there is missing data, you can now call *dmgr newpars* which will send you back to Step 7.
12. *splitmissing remote1,remote2,remote3* - If you want to split parameter files across multiple clusters, use this command. It will split a parameter file of missing data into multiple files. Follow instructions for switching clusters if you do this.
13. *refine* Call the *refine* script for the experiment, which does post-processing on the raw data. At this point you should make your plots and check that the results make sense. Record the results in the file $DATAMGR\_DATA/[CODE]/[CODE].txt.
14. *cleanscratch* - Delete the data files on the remote server.
15. *pull* - Pull the project files back from the remote to the local machine, and delete all files on the remote server.

#### Switching Clusters
In some cases you will want to generate data on multiple clusters for a single experiment. This requires generating multiple project files via *stashing*. Pick a name for the stashfile and use the command *dmgr [CODE] stash [stashname]*. This pulls code from the remote machine, stashes the configuration file, and resets the experiment to the 'select' stage of the pipeline. You may now select a new cluster. To recover a stashed configuration, use the command *dmgr [CODE] unstash [stashname]* and to see the available stashes for the 'stashname' parameter, use the command *dmgr [CODE] showstash*. Once you are done with a stashfile, you may delete it via *dmgr [CODE] delstash [stashname]*.

#### Switching Experiments
Suppose you want to return to a previous experiment to regenerate data. You can do this by stashing your current configuration and then running *dmgr [CODE] setexperiment [experiment]* where 'experiment' is a number. Alternatively, use *dmgr [CODE] new* to start a new experiment for this project. Switch back to the original experiment using the rules for stashing and unstashing.

#### Modifications
Likely you will find that you want to tweak bits and pieces to match your naming schemes, cluster rules, and personal coding style.  If something isn't working, take a look inside the 'dmgr' script to see what's happening.

### SLURM
The scripts provided are intended to be used with a cluster managed by SLURM, though with some edits you should be able to make this work with other cluster management software, such as LSF or PBS. Files which interface with SLURM on the remote machines are installed in the sample project's subdirectory $DATAMGR\_DATA/ABC/slurm.  These should be tweaked by hand to fit the requirements of your clusters. Usually I store one version per project, and they live in the project directory with the source code.

## Maintenance
Please report comments or bugs to Will Cunningham at \<wjcunningham7 AT gmail DOT com\> 

&copy; Will Cunningham 2020
